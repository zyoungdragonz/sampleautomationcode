package google.app.tests.teststates;

import constants.ConfigurationConstants;
import driver.DriverFactory;
import driver.DriverType;
import driver.IDriverManager;
import gherkin.formatter.Formatter;
import gherkin.formatter.model.Background;
import gherkin.formatter.model.Examples;
import gherkin.formatter.model.Feature;
import gherkin.formatter.model.Scenario;
import gherkin.formatter.model.ScenarioOutline;
import gherkin.formatter.model.Step;
import org.openqa.selenium.WebDriver;
import utils.DateTimeUtils;
import utils.PropertiesUtils;

import java.util.List;
import java.util.logging.Logger;

public class GoogleAppFormatter implements Formatter {
    private static final Logger LOGGER = Logger.getLogger(GoogleAppFormatter.class.getName());

    private WebDriver driver;

    @Override
    public void syntaxError(String s, String s1, List<String> list, String s2, Integer integer) {

    }

    @Override
    public void uri(String s) {

    }

    @Override
    public void feature(Feature feature) {
        LOGGER.info("Starting Karros App Test >>>>>>>>> Feature: " + feature.getName());
        LOGGER.info("Start the test at: " + DateTimeUtils.getCurrentDateTime());
        PropertiesUtils.loadProperties(ConfigurationConstants.RESOURCE_TESTING_CONFIGURATION);
    }

    @Override
    public void scenarioOutline(ScenarioOutline scenarioOutline) {

    }

    @Override
    public void examples(Examples examples) {

    }

    @Override
    public void startOfScenarioLifeCycle(Scenario scenario) {
        IDriverManager driverManager = DriverFactory.getDriverManager(DriverType.CHROME);
        driverManager.setDriver();
        driver = driverManager.getDriver();
        driver.manage().window().maximize();
    }

    @Override
    public void background(Background background) {

    }

    @Override
    public void scenario(Scenario scenario) {

    }

    @Override
    public void step(Step step) {

    }

    @Override
    public void endOfScenarioLifeCycle(Scenario scenario) {
        if(driver != null) {
            driver.quit();
        }
    }

    @Override
    public void done() {
    }

    @Override
    public void close() {
    }

    @Override
    public void eof() {
        LOGGER.info("End the test at: " + DateTimeUtils.getCurrentDateTime());
    }
}
