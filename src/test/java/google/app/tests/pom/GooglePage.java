package google.app.tests.pom;

import constants.ConfigurationConstants;

public class GooglePage extends CommonPage {

    public void getHere() {
        driver.get(System.getProperty(ConfigurationConstants.GOOGLE_WEBAPP_URL));
    }

}
