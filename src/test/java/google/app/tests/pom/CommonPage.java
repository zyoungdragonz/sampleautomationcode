package google.app.tests.pom;

import constants.ConfigurationConstants;
import driver.DriverFactory;
import driver.DriverType;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

abstract class CommonPage {

    WebDriver driver;

    CommonPage() {
        driver = DriverFactory.getDriverManager(DriverType.CHROME).getDriver();
        driver.manage().timeouts().implicitlyWait(ConfigurationConstants.GOOGLE_TIMEOUT_WAIT_ELEMENT, TimeUnit.SECONDS);
    }
}
