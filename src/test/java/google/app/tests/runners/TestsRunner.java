package google.app.tests.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
        format = {"pretty", "html:target/cucumber"},
        features = {"src/test/java/google/app/tests/features/GoogleTest.feature"},
        plugin = {"GoogleAppFormatter"},
        glue = "google/app/tests/steps")
public class TestsRunner extends AbstractTestNGCucumberTests {
}
