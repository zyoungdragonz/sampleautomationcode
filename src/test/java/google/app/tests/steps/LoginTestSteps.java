package google.app.tests.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import google.app.tests.data.CommonTestData;
import google.app.tests.pom.GooglePage;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class LoginTestSteps {

    private GooglePage googlePage = new GooglePage();

    @Given("^user navigates to Google page$")
    public void userNavigatesToGooglePage() {
        googlePage.getHere();
    }

}
