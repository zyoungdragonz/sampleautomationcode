package driver;

import org.openqa.selenium.WebDriver;

public interface IDriverManager {
    WebDriver getDriver();

    void setDriver();
}
