package driver;

public class DriverFactory {
    private DriverFactory() {
    }

    private static IDriverManager iDriver;

    public static IDriverManager getDriverManager(DriverType driverType) {
        switch(driverType) {
            case CHROME:
                iDriver = new ChromeDriverManager();
                break;
            case FIREFOX:
                //Firefox driver in here
                break;
            default:
                iDriver = new ChromeDriverManager();
        }
        return iDriver;
    }

}
