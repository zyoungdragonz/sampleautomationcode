package driver;

import constants.DriverConstants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeDriverManager implements IDriverManager {

    private static final ThreadLocal<WebDriver> chromeDriverThreadLocal = new ThreadLocal<>();

    public WebDriver getDriver() {
        if(chromeDriverThreadLocal.get() == null) {
            setDriver();
        }
        return chromeDriverThreadLocal.get();
    }

    public void setDriver() {
        System.setProperty(DriverConstants.WEB_DRIVER_CHROME_DRIVER, DriverConstants.WEB_DRIVER_CHROME_DRIVER_PATH);
        chromeDriverThreadLocal.set(new ChromeDriver());
    }
}
