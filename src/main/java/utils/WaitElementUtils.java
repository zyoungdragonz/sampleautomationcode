package utils;

import constants.ConfigurationConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class WaitElementUtils {
    private WaitElementUtils() {
    }

    public static WebElement waitForPresenceOfElement(WebDriver driver, By by) {
        return new WebDriverWait(driver, ConfigurationConstants.GOOGLE_TIMEOUT_WAIT_ELEMENT)
                .until(ExpectedConditions.presenceOfElementLocated(by));
    }

    public static List<WebElement> waitForPresenceOfElements(WebDriver driver, By by) {
        return new WebDriverWait(driver, ConfigurationConstants.GOOGLE_TIMEOUT_WAIT_ELEMENT)
                .until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
    }

    public static boolean waitForInvisibilityOfElement(WebDriver driver, By by) {
        return new WebDriverWait(driver, ConfigurationConstants.GOOGLE_TIMEOUT_WAIT_ELEMENT)
                .until(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    public static List<String> getTextOfElement(WebDriver driver, By by) {
        List<String> textList = new ArrayList<>();
        List<WebElement> elementList = new WebDriverWait(driver, ConfigurationConstants.GOOGLE_TIMEOUT_WAIT_ELEMENT)
                .until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
        for(int i = 0; i < elementList.size(); i++) {
            textList.add(i, elementList.get(i).getText());
        }
        return textList;
    }

    public static Object executeJsScripts(WebDriver driver, String scripts) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        return js.executeScript(scripts);
    }

    public static void waitFor(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch(InterruptedException e) {
            e.printStackTrace();
        }
    }
}
