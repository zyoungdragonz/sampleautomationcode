package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

public class PropertiesUtils {
    private static final Logger LOGGER = Logger.getLogger(PropertiesUtils.class.getName());

    private PropertiesUtils() {
    }

    public static void loadProperties(String strConfigDic) {
        String strConfigDirectory = new File(System.getProperty("user.dir")) + strConfigDic;
        File directory = new File(strConfigDirectory);
        File[] fList = directory.listFiles();
        assert fList != null;
        for(File file : fList) {
            String strFileName = file.getAbsolutePath();
            try {
                InputStream is = new FileInputStream(strFileName);
                Throwable var1 = null;
                try {
                    System.getProperties().load(is);
                } catch(IOException var2) {
                    var1 = var2;
                    throw var2;
                } finally {
                    if(var1 != null) {
                        try {
                            is.close();
                        } catch(IOException e1) {
                            var1.addSuppressed(e1);
                        }
                    } else {
                        is.close();
                    }
                }
            } catch(IOException e2) {
                LOGGER.info(e2.getMessage());
            }
        }
    }
}
