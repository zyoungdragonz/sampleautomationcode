package utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateTimeUtils {

    private static final String DATE_TIME_COMMON_PATTERN = "yyyy_MM_dd___HH_mm_ss";

    private DateTimeUtils() {
    }

    public static String getCurrentDateTime() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_TIME_COMMON_PATTERN);
        return sdf.format(cal.getTime());
    }
}
