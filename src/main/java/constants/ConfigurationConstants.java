package constants;

public final class ConfigurationConstants {
    private ConfigurationConstants() {
    }

    public static final String RESOURCE_TESTING_CONFIGURATION = "/src/main/resources/configuration";

    public static final String GOOGLE_WEBAPP_URL = "google.webapp.url";

    public static final long GOOGLE_TIMEOUT_WAIT_ELEMENT = Integer.parseInt(System.getProperty("google.timeout.waitelement"));
}
