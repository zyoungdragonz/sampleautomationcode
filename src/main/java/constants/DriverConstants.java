package constants;

public final class DriverConstants {
    private DriverConstants() {
    }

    private static final String USER_DIRECTORY = System.getProperty("user.dir");

    public static final String WEB_DRIVER_CHROME_DRIVER = "webdriver.chrome.driver";

    public static final String WEB_DRIVER_CHROME_DRIVER_PATH = USER_DIRECTORY + System.getProperty("chrome.driver.path");
}
