# Google Sample Test
I. Manual testing:
    None

II. Automation testing:

	a. Pre-condition:
	    _Run on Window platform due to using WebDriver for window
		_JDK 1.8 or above is installed on your local machine
		_Maven is installed on your local machine
		_Install any IDE tool you like to trigger the test easily, suggest Intellj IDE Community latest version
		_Clone this source code onto your local machine
		_Open source code using IDE tool then wait until all dependencies are dowloaded
		_Make sure the source code is compiled successfully
	b. How to run the test:
		_Run by **TestsRunner.class** at karros/src/test/java/google/app/tests/runners
	c. Report:
	    _After finish the test, let take a look the raw report at: target/cucumber/